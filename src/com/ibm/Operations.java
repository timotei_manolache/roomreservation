package com.ibm;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import javax.swing.plaf.synth.SynthScrollBarUI;

import com.ibm.building.AccessType;
import com.ibm.building.Room;
import com.ibm.building.Structure;
import com.ibm.resources.Blackboard;
import com.ibm.resources.Phone;
import com.ibm.resources.Printer;
import com.ibm.util.Constants;

public class Operations {

	protected static void loadPrintersFromFile() {
		Constants.building.printers = new ArrayList<>();
		BufferedReader br;
		File printersFile = new File("printer.csv");
		try {
			String thisLine = "";
			FileInputStream fis = new FileInputStream(printersFile);
			br = new BufferedReader(new InputStreamReader(fis));
			while ((thisLine = br.readLine()) != null) {
				Constants.building.printers.add(makePrintersFromCSV(thisLine));
			}
			br.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static Printer makePrintersFromCSV(String thisLine) {
		Printer printer = new Printer();
		String[] lineValues = thisLine.split(",");
		printer.setInventoryNumber(lineValues[0]);
		printer.setIp(lineValues[1]);
		printer.setColour(Boolean.parseBoolean(lineValues[2]));
		return printer;
	}

	protected static void loadBlackboardsFromFile() {
		Constants.building.blackboards = new ArrayList<>();
		BufferedReader br;
		File blackboardsFile = new File("blackboards.csv");
		try {
			String thisLine = "";
			FileInputStream is = new FileInputStream(blackboardsFile);
			br = new BufferedReader(new InputStreamReader(is));
			while ((thisLine = br.readLine()) != null) {
				Constants.building.blackboards.add(makeBlackboardsFromCSV(thisLine));
			}
			br.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static Blackboard makeBlackboardsFromCSV(String thisLine) {
		Blackboard bboard = new Blackboard();
		String[] lineValues = thisLine.split(",");
		bboard.setInventoryNumber(lineValues[0]);
		bboard.setSmartBoard(Boolean.parseBoolean(lineValues[1]));
		return bboard;
	}

	protected static void loadPhonesFromFile() {
		Constants.building.phones = new ArrayList<>();
		BufferedReader br;
		File phonesFile = new File("phones.csv");
		try {
			String thisLine = "";
			FileInputStream is = new FileInputStream(phonesFile);
			br = new BufferedReader(new InputStreamReader(is));
			while ((thisLine = br.readLine()) != null) {
				Constants.building.phones.add(makePhoneFromCSV(thisLine));
			}
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	protected static Phone makePhoneFromCSV(String thisLine) {
		Phone phone = new Phone();
		String[] lineValues = thisLine.split(",");
		phone.setInventoryNumber(lineValues[0]);
		phone.setType(lineValues[1]);
		return phone;
	}

	protected static void loadRoomsFromFile() {
		Constants.building.rooms = new ArrayList<>();
		BufferedReader br;
		File roomsFile = new File("room.csv");
		try {
			String thisLine = "";
			FileInputStream is = new FileInputStream(roomsFile);
			br = new BufferedReader(new InputStreamReader(is));
			while ((thisLine = br.readLine()) != null) {
				Constants.building.rooms.add(makeRoomFromCSV(thisLine));
			}
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static Room makeRoomFromCSV(String thisLine) {
		Room room = new Room();
		String[] lineValues = thisLine.split(",");
		room.setNume(lineValues[0]);
		room.setFloor(Integer.parseInt(lineValues[1]));
		room.setCapacity(Integer.parseInt(lineValues[2]));
		room.setNetworkConnection(Boolean.parseBoolean(lineValues[3]));
		room.setPhoneAvailable(Boolean.parseBoolean(lineValues[4]));
		room.setAvailable(Boolean.parseBoolean(lineValues[5]));
		room.setAccessType(lineValues[6]);
		room.setPrinter(getPrinterById(lineValues[7]));
		room.setBlackboard(getBlackboardById(lineValues[8]));
		room.setPhone(getPhoneByID(lineValues[9]));
		return room;
	}

	private static Reservation makeReservationsFromCSV(String thisLine) {
		Reservation res = new Reservation();
		String[] lineValues = thisLine.split(",");
		res.setRequestorName(lineValues[0]);
		res.setSelectedRoom(getRoomByName(lineValues[1]));
		res.setParticipantsNumber(Integer.parseInt(lineValues[2]));
		String start = lineValues[3];
		String stop = lineValues[4];
		DateFormat format = new SimpleDateFormat("HH:mm", Locale.ENGLISH);
		try {
			Date dateStart = format.parse(start);
			Date dateStop = format.parse(stop);
			res.setStartDateAndTime(dateStart);
			res.setEndDateAndTime(dateStop);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return res;
	}

	protected static Printer getPrinterById(String string) {
		// int intID = Integer.parseInt(string);
		for (Printer p : Constants.building.printers) {
			if (p.getInventoryNumber().equals(string)) {
				return p;
			}
		}
		return null;
	}

	protected static Blackboard getBlackboardById(String string) {
		for (Blackboard b : Constants.building.blackboards) {
			if (b.getInventoryNumber().equals(string)) {
				return b;
			}
		}
		return null;
	}

	protected static Phone getPhoneByID(String string) {
		// int intID = Integer.parseInt(string);

		for (Phone p : Constants.building.phones) {
			if (p.getInventoryNumber().equals(string)) {
				return p;
			}
		}
		return null;
	}

	protected static Room getRoomByName(String name) {
		for (Room r : Constants.building.rooms) {
			if (r.getNume().equals(name)) {
				return r;
			}
		}
		return null;
	}

	protected static void loadReservationsFromFile() {
		Constants.building.reservations = new ArrayList<>();
		BufferedReader br;
		File reservationsFile = new File("reservations.csv");
		try {
			String thisLine = "";
			FileInputStream is = new FileInputStream(reservationsFile);
			br = new BufferedReader(new InputStreamReader(is));
			while ((thisLine = br.readLine()) != null) {
				Constants.building.reservations.add(makeReservationsFromCSV(thisLine));
			}
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static Room createRoom(String command, String nume, int floor, int capacity, boolean networkConnection,
			boolean phoneAvailable, boolean available, String accessType, Printer printer, Blackboard blackbboard,
			Phone phone) {
		if (command.equals(Constants.CREATE_ROOM_WITH_NAME_AND_CAPACITY)) {
			Room r = new Room(nume, floor, capacity, networkConnection, phoneAvailable, available, accessType, printer,
					blackbboard, phone);
			System.out.println("Room was created.");
			Constants.building.rooms.add(r);
			System.out.println("Room was added to structure.");
			return r;

		} else {
			System.out.println("Error while creating room");
			return null;
		}
	}

	public static Printer createPrinter(String command, String id, String ip, boolean isColor) {
		if (command.equals(Constants.CREATE_NEW_PRINTER)) {
			Printer p = new Printer(id, ip, isColor);
			System.out.println("Printer was created.");
			Constants.building.printers.add(p);
			System.out.println("Printer was added to structure.");
			return p;
		} else {
			System.out.println("Error while creating printer");
			return null;
		}
	}

	public static Blackboard createBlackboard(String command, String id, boolean isSmartBoard) {
		if (command.equals(Constants.CREATE_NEW_BLACKBOARD)) {
			Blackboard b = new Blackboard(id, isSmartBoard);
			System.out.println("Blackboard was created.");
			Constants.building.blackboards.add(b);
			System.out.println("Blackboard was added to structure");
			return b;
		} else {
			System.out.println("Error while creating blackboard");
			return null;
		}
	}

	public static Phone createPhone(String command, String id, String type) {
		if (command.equals(Constants.CREATE_NEW_PHONE)) {
			Phone p = new Phone(id, type);
			System.out.println("Phone was created");
			Constants.building.phones.add(p);
			System.out.println("Phone was added to structure");
			return p;
		} else {
			System.out.println("Error while creating phone");
			return null;
		}
	}

	public static void getCapacityOfRoom(String command, String name) {
		if (command.equals(Constants.GET_CAPACITY_FOR_ROOM)) {
			for (Room room : Constants.building.rooms) {
				if (room.getNume().equals(name)) {
					System.out.println("The room has a capacity of " + room.getCapacity() + " persons.");
				} else {
					System.out.println("Searching for room...");
				}
			}
		} else {
			System.out.println("Room does not exist.");
		}
		System.out.println("Wrong command.");
	}

	public static Reservation setReservation(String command, String reqName, Room roomName, int noPart, Date startH,
			Date endH) {
		if (command.equals(Constants.CREATE_RESERVATION)) {
			Reservation r = new Reservation(startH, endH, noPart, roomName, reqName);
			System.out.println("Reservation was created");
			Constants.building.reservations.add(r);
			System.out.println("Reservation was added to structure");
			return r;
		} else {
			System.out.println("Error while creating reservation");
			return null;
		}
	}

	public static void getReservation(String command) {
		if (command.equals(Constants.VIEW_RESERVED_ROOMS)) {
			for (Reservation r : Constants.building.reservations) {
				System.out.println(getRoomByName(r.getSelectedRoom().getNume()).getNume());
			}
		} else {
			System.out.println("No reservations.");
		}
	}

	public static void deleteReservation(String command, String name) {
		if (command.equals(Constants.DELETE_RESERVATION)) {
			for (Reservation r : Constants.building.reservations) {
				if (r.getRequestorName().equals(name)) {
					Constants.building.reservations.remove(r);
					System.out.println("Reservation removed.");
				} else {
					System.out.println("Reservation does not exist.");
				}
			}
			WriteCSV.writeReservationCSV("reservations.csv");
			System.out.println("S-a rescris si fisierul.");
		}

	}

	public static void viewRooms(String command) {
		if (command.equals(Constants.VIEW_AVAILABLE_ROOMS)) {
			for (Room r : Constants.building.rooms) {
				for (Reservation res : Constants.building.reservations) {
					if (!r.getNume().equals(res.getSelectedRoom().getNume())) {
						System.out.println("Room is not free.");
					} else {
						System.out.println("The room " + r.getNume() + " is free.");
					}
				}
			}
		}
	}

}
