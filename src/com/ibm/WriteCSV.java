package com.ibm;

import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import com.ibm.building.Room;
import com.ibm.building.Structure;
import com.ibm.resources.Blackboard;
import com.ibm.resources.Phone;
import com.ibm.resources.Printer;

public class WriteCSV {
	private static final String COMMA = ",";
	private static final String ENDL = "\n";
	static FileWriter fileWriter = null;

	protected static void writeRoomCSV(String fileName) {
		try {
			fileWriter = new FileWriter(fileName);

			for (Room room : Structure.rooms) {
				fileWriter.append(room.getNume());
				fileWriter.append(COMMA);
				fileWriter.append(String.valueOf(room.getFloor()));
				fileWriter.append(COMMA);
				fileWriter.append(String.valueOf(room.getCapacity()));
				fileWriter.append(COMMA);
				fileWriter.append(String.valueOf(room.isNetworkConnection()));
				fileWriter.append(COMMA);
				fileWriter.append(String.valueOf(room.isPhoneAvailable()));
				fileWriter.append(COMMA);
				fileWriter.append(String.valueOf(room.isAvailable()));
				fileWriter.append(COMMA);
				fileWriter.append(String.valueOf(room.getAccessType()));
				fileWriter.append(COMMA);
				fileWriter.append(room.getPrinter().getInventoryNumber());
				fileWriter.append(COMMA);
				fileWriter.append(room.getBlackboard().getInventoryNumber());
				fileWriter.append(COMMA);
				fileWriter.append(room.getPhone().getInventoryNumber());
				fileWriter.append(ENDL);
			}
			System.out.println("CSV created succesfully");
		} catch (IOException e) {
			System.out.println("CSV not created succesfully");
			e.printStackTrace();
		} finally {
			try {
				fileWriter.flush();
				fileWriter.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	protected static void writeReservationCSV(String fileName){
		try {
			fileWriter = new FileWriter(fileName);
			DateFormat df = new SimpleDateFormat("HH:mm");
			
			for (Reservation res : Structure.reservations){
				fileWriter.append(res.getRequestorName());
				fileWriter.append(COMMA);
				fileWriter.append(String.valueOf(res.getSelectedRoom().getNume()));
				fileWriter.append(COMMA);
				fileWriter.append(String.valueOf(res.getParticipantsNumber()));
				fileWriter.append(COMMA);
				String startH = df.format(res.getStartDateAndTime());
				fileWriter.append(startH);
				fileWriter.append(COMMA);
				String endH = df.format(res.getEndDateAndTime());
				fileWriter.append(endH);
				fileWriter.append(ENDL);
				
			}
			System.out.println("CSV created succesfully");
		} catch (IOException e) {
			System.out.println("CSV not created succesfully");
			e.printStackTrace();
		} finally {
			try {
				fileWriter.flush();
				fileWriter.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	protected static void writePrinterCSV(String fileName){
		try{
			fileWriter = new FileWriter(fileName);
			
			for (Printer printer : Structure.printers){
				fileWriter.append(String.valueOf(printer.getInventoryNumber()));
				fileWriter.append(COMMA);
				fileWriter.append(String.valueOf(printer.getIp()));
				fileWriter.append(COMMA);
				fileWriter.append(String.valueOf(printer.isColour()));
				fileWriter.append(ENDL);
			}
			System.out.println("CSV created sucessfully");
		} catch (IOException e) {
			System.out.println("CSV not created succesfully");
			e.printStackTrace();
		} finally {
			try {
				fileWriter.flush();
				fileWriter.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	protected static void writePhoneCSV(String fileName) {
		try {
			fileWriter = new FileWriter(fileName);
			
			for(Phone phone : Structure.phones) {
				fileWriter.append(String.valueOf(phone.getInventoryNumber()));
				fileWriter.append(COMMA);
				fileWriter.append(String.valueOf(phone.getType()));
				fileWriter.append(ENDL);
			}
			System.out.println("CSV created succesfully");
		} catch (IOException e) {
			System.out.println("CSV not created succesfully");
			e.printStackTrace();
		} finally {
			try {
				fileWriter.flush();
				fileWriter.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	protected static void writeBlackboardCSV(String fileName) {
		try {
			fileWriter = new FileWriter(fileName);
			
			for (Blackboard board : Structure.blackboards){
				fileWriter.append(String.valueOf(board.getInventoryNumber()));
				fileWriter.append(COMMA);
				fileWriter.append(String.valueOf(board.isSmartBoard()));
				fileWriter.append(ENDL);
			}
			System.out.println("CSV created succesfully");
		} catch (IOException e) {
			System.out.println("CSV not created succesfully");
			e.printStackTrace();
		} finally {
			try {
				fileWriter.flush();
				fileWriter.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
