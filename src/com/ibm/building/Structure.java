package com.ibm.building;

import java.util.ArrayList;
import java.util.List;

import com.ibm.Reservation;
import com.ibm.resources.Blackboard;
import com.ibm.resources.Phone;
import com.ibm.resources.Printer;

public class Structure {
	public static List<Room> rooms = new ArrayList<>();
	public static List<Phone> phones = new ArrayList<>();
	public static List<Reservation> reservations = new ArrayList<>();
	public static List<Blackboard> blackboards = new ArrayList<>();
	public static List<Printer> printers = new ArrayList<>();
	public static List<Room> reservedRooms = new ArrayList<>();
}
