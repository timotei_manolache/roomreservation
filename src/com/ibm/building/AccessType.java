package com.ibm.building;

public enum AccessType {
	PUBLIC, RESTRICTED
}
