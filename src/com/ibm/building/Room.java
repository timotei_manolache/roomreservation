package com.ibm.building;

import java.io.Serializable;

import com.ibm.resources.Blackboard;
import com.ibm.resources.Phone;
import com.ibm.resources.Printer;

public class Room implements Serializable {
	private String nume;
	private int floor;
	private int capacity;
	private boolean networkConnection;
	private boolean phoneAvailable;
	private boolean available;
	private AccessType accessType;
	private Printer printer;
	private Blackboard blackboard;
	private Phone phone;

	public Room(String nume, int floor, int capacity, boolean networkConnection, boolean phoneAvailable,
			boolean available, String accessType, Printer printer, Blackboard blackboard, Phone phone) {
		super();
		this.nume = nume;
		this.floor = floor;
		this.capacity = capacity;
		this.networkConnection = networkConnection;
		this.phoneAvailable = phoneAvailable;
		this.available = available;
		this.setAccessType(accessType);
		this.setPrinter(printer);
		this.setBlackboard(blackboard);
		this.setPhone(phone);
	}

	public Room() {

	}

	public String getNume() {
		return nume;
	}

	public void setNume(String nume) {
		this.nume = nume;
	}

	public int getCapacity() {
		return capacity;
	}

	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}

	public int getFloor() {
		return floor;
	}

	public void setFloor(int floor) {
		this.floor = floor;
	}

	public boolean isNetworkConnection() {
		return networkConnection;
	}

	public void setNetworkConnection(boolean networkConnection) {
		this.networkConnection = networkConnection;
	}

	public boolean isPhoneAvailable() {
		return phoneAvailable;
	}

	public void setPhoneAvailable(boolean phoneAvailable) {
		this.phoneAvailable = phoneAvailable;
	}

	public boolean isAvailable() {
		return available;
	}

	public void setAvailable(boolean available) {
		this.available = available;
	}

	public AccessType getAccessType() {
		return accessType;
	}

	public void setAccessType(AccessType accessType) {
		this.accessType = accessType;
	}

	public Printer getPrinter() {
		return this.printer;
	}

	public void setPrinter(Printer printer) {
		this.printer = printer;
	}

	public Blackboard getBlackboard() {
		return this.blackboard;
	}

	public void setBlackboard(Blackboard blackboard) {
		this.blackboard = blackboard;
	}

	public Phone getPhone() {
		return this.phone;

	}

	public void setPhone(Phone phone) {
		this.phone = phone;
	}

	@Override
	public String toString() {
		return "Room [nume=" + nume + ", floor=" + floor + ", capacity=" + capacity + ", networkConnection="
				+ networkConnection + ", phoneAvailable=" + phoneAvailable + ", available=" + available
				+ ", accessType=" + accessType + ", printer=" + printer + ", blackboard=" + blackboard + ", phone="
				+ phone + "]";
	}

	public void setAccessType(String string) {
		if (string.equals("PUBLIC")) {
			this.accessType = AccessType.PUBLIC;
		} else if (string.equals("RESTRICTED")) {
			this.accessType = AccessType.RESTRICTED;
		} else {
			System.out.println("Invalid access type");
		}
	}
}
