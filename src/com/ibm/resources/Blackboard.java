package com.ibm.resources;

public class Blackboard extends BaseEntity {
	private boolean smartBoard;

	public Blackboard(String id, boolean isSmartBoard) {
		super();
		this.inventoryNumber = id;
		this.smartBoard = isSmartBoard;
	}

	public Blackboard() {

	}

	public boolean isSmartBoard() {
		return this.smartBoard;
	}

	public void setSmartBoard(boolean smartBoard) {
		this.smartBoard = smartBoard;
	}
}
