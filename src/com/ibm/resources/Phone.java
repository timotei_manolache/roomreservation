package com.ibm.resources;

public class Phone extends BaseEntity {
	private Type type;

	enum Type {
		ANALOGIC, VOIP
	}

	public Phone() {

	}

	public Phone(String id, String type) {
		this.inventoryNumber = id;
		if (type.equals(Type.VOIP.toString())) {
			this.type = Type.VOIP;
		} else {
			this.type = Type.ANALOGIC;
		}
	}

	public String getType() {
		return type.name();
	}

	public void setType(String type) {
		if (type.equals(Type.VOIP.name())) {
			this.type = Type.VOIP;
		} else {
			this.type = Type.ANALOGIC;
		}
	}
	
	@Override
	public String toString() {
		return "Phone with the id " + this.inventoryNumber + " is an " + this.type + " type.";
	}
}
