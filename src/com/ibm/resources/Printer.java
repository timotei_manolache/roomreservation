package com.ibm.resources;

public class Printer extends BaseEntity {
	private String ip;
	private boolean colour;

	public Printer(String id, String ip, boolean colour) {
		super();
		this.inventoryNumber = id;
		this.ip = ip;
		this.colour = colour;
	}

	public Printer() {

	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public boolean isColour() {
		return colour;
	}

	public void setColour(boolean colour) {
		this.colour = colour;
	}
}
