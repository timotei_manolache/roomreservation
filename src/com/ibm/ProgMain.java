package com.ibm;

import java.awt.List;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.Locale;

import com.ibm.building.Room;
import com.ibm.building.Structure;
import com.ibm.resources.Blackboard;
import com.ibm.resources.Phone;
import com.ibm.resources.Printer;
import com.ibm.util.Constants;

public class ProgMain {

	static {
		Constants.operations.loadPrintersFromFile();
		Constants.operations.loadBlackboardsFromFile();
		Constants.operations.loadPhonesFromFile();
		Constants.operations.loadRoomsFromFile();
		Constants.operations.loadReservationsFromFile();
	}

	public static void main(String[] args) {

		// for (Printer p : Constants.building.printers) {
		// System.out.println("Printer id: " + p.getInventoryNumber());
		// System.out.println("Printer ip: " + p.getIp());
		// System.out.println("Printer isColor: " + p.isColour());
		// }
		//
		// for (Blackboard b : Constants.building.blackboards) {
		// System.out.println("Bk id: " + b.getInventoryNumber());
		// System.out.println("Bk is SmartB: " + b.isSmartBoard());
		// }
		//
		// for (Phone p : Constants.building.phones) {
		// System.out.println("Phone id: " + p.getInventoryNumber());
		// System.out.println("Phone type: " + p.getType());
		// }
		//
		// for (Room r : Constants.building.rooms) {
		// System.out.println("Sala: " + r.getNume());
		// System.out.println("\t etaj: " + r.getFloor());
		// System.out.println("\t capacitate: " + r.getCapacity());
		// System.out.println("\t internet: " + r.isNetworkConnection());
		// System.out.println("\t hasPhone: " + r.isPhoneAvailable());
		// System.out.println("\t isAvailable: " + r.isAvailable());
		// System.out.println("\t type: " + r.getAccessType());
		// System.out.println("\t phone: ");
		// if (r.getPhone() != null) {
		// System.out.println("\t\t" + r.getPhone().getInventoryNumber() + " - "
		// + r.getPhone().getType());
		// } else {
		// System.out.println("\t\t no phone available");
		// }
		// if (r.getBlackboard() != null) {
		// System.out.println("\t\t" + r.getBlackboard().getInventoryNumber() +
		// " - " + r.getBlackboard().isSmartBoard());
		// } else {
		// System.out.println("\t\t no bkboard available");
		// }
		// if (r.getPrinter() != null) {
		// System.out.println("\t\t" + r.getPrinter().getInventoryNumber() + " -
		// " + r.getPrinter().getIp() + " - " + r.getPrinter().isColour());
		// } else {
		// System.out.println("\t\t no printer available");
		// }
		// }
		//
		// for (Reservation r : Constants.building.reservations) {
		// System.out.println("Reservation requestor: " + r.getRequestorName());
		// System.out.println("Reserved room: " + r.getSelectedRoom());
		// System.out.println("Reservation start: " + r.getStartDateAndTime());
		// System.out.println("Reservation end: " + r.getEndDateAndTime());
		// System.out.println("Reservation participants: " +
		// r.getParticipantsNumber());
		// }

		ArrayList<String> Simur = new ArrayList<String>();
		Simur.add("Manea Cristian");
		Simur.add("Rizea Andrei");
		Simur.add("Manolache Cosmin");
		Simur.add("Mihalcea Georgian");
		Simur.add("Nistor Simona");
		Simur.add("Parvu Mihaela");
		Simur.add("Muscalu Alexandru");
		
		
		FileWriter writer = null;
		try {
			writer = new FileWriter("output.txt");
			for(String str: Simur) {
				writer.write(str);
			}
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		
		
		
//		switch (args[0]) {
//		case Constants.CREATE_ROOM_WITH_NAME_AND_CAPACITY:
//			if (args.length < 11) {
//				System.out.println("Not enough arguments");
//			}
//			if (args.length > 11) {
//				System.out.println("Too many arguments");
//			} else {
//				System.out.println("Argument nr correct.");
//				Operations.createRoom(args[0], args[1], Integer.parseInt(args[2]), Integer.parseInt(args[3]),
//						Boolean.parseBoolean(args[4]), Boolean.parseBoolean(args[5]), Boolean.parseBoolean(args[6]),
//						args[7], Operations.getPrinterById(args[8]), Operations.getBlackboardById(args[9]),
//						Operations.getPhoneByID(args[10]));
//				WriteCSV.writeRoomCSV("room.csv");
//			}
//			break;
//		case Constants.GET_CAPACITY_FOR_ROOM:
//			if (args.length < 2) {
//				System.out.println("Not enough arguments");
//			}
//			if (args.length > 2) {
//				System.out.println("Too many arguments");
//			} else {
//				System.out.println("Argument nr correct.");
//				Operations.getCapacityOfRoom(args[0], args[1]);
//			}
//			break;
//		case Constants.CREATE_NEW_PRINTER:
//			if (args.length < 4) {
//				System.out.println("Not enough arguments");
//			}
//			if (args.length > 4) {
//				System.out.println("Too many arguments");
//			} else {
//				System.out.println("Argument nr correct.");
//				Operations.createPrinter(args[0], args[1], args[2], Boolean.parseBoolean(args[3]));
//				WriteCSV.writePrinterCSV("printer.csv");
//			}
//			break;
//		case Constants.CREATE_NEW_BLACKBOARD:
//			if (args.length < 3) {
//				System.out.println("Not enough arguments");
//			}
//			if (args.length > 3) {
//				System.out.println("Too many arguments");
//			} else {
//				System.out.println("Argument nr correct.");
//				Operations.createBlackboard(args[0], args[1], Boolean.parseBoolean(args[2]));
//				WriteCSV.writeBlackboardCSV("blackboards.csv");
//			}
//			break;
//		case Constants.CREATE_NEW_PHONE:
//			if (args.length < 3) {
//				System.out.println("Not enough arguments");
//			}
//			if (args.length > 3) {
//				System.out.println("Too many arguments");
//			} else {
//				System.out.println("Argument nr correct.");
//				Operations.createPhone(args[0], args[1], args[2]);
//				WriteCSV.writePhoneCSV("phones.csv");
//			}
//			break;
//		case Constants.CREATE_RESERVATION:
//			if (args.length < 6) {
//				System.out.println("Not enough arguments");
//			}
//			if (args.length > 6) {
//				System.out.println("Too many arguments");
//			} else {
//				System.out.println("Argument nr correct.");
//				String start = args[4];
//				String stop = args[5];
//				DateFormat format = new SimpleDateFormat("HH:mm", Locale.ENGLISH);
//				try {
//					Date dateStart = format.parse(start);
//					Date dateStop = format.parse(stop);
//					Operations.setReservation(args[0], args[1], Operations.getRoomByName(args[2]),
//							Integer.parseInt(args[3]), dateStart, dateStop);
//					WriteCSV.writeReservationCSV("reservations.csv");
//				} catch (ParseException e) {
//					e.printStackTrace();
//				} catch (NullPointerException e) {
//					e.printStackTrace();
//				}
//			}
//			break;
//		case Constants.VIEW_RESERVED_ROOMS:
//			if (args.length < 1) {
//				System.out.println("Not enough arguments");
//			}
//			if (args.length > 1) {
//				System.out.println("Too many arguments");
//			} else {
//				System.out.println("Argument nr correct.");
//				Operations.getReservation(args[0]);
//			}
//			break;
//		case Constants.DELETE_RESERVATION:
//			if (args.length < 2) {
//				System.out.println("Not enough arguments");
//			}
//			if (args.length > 2) {
//				System.out.println("Too many arguments");
//			} else {
//				System.out.println("Argument nr correct");
//				Operations.deleteReservation(args[0], args[1]);
//			}
//			break;
//		case Constants.VIEW_AVAILABLE_ROOMS:
//			if (args.length < 1) {
//				System.out.println("Not enough arguments");
//
//			}
//			if (args.length > 1) {
//				System.out.println("Too many arguments");
//			} else {
//				System.out.println("Argument nr correct.");
//				Operations.viewRooms(args[0]);
//			}
//			break;
//		default:
//			System.out.println("Wrong command!");
//			break;
//		}
	}

	private static BufferedWriter BufferedWriter(FileWriter fw) {
		// TODO Auto-generated method stub
		return null;
	}
}
