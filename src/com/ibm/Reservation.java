package com.ibm;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.ibm.building.Room;

public class Reservation {
	private Date startDateAndTime;
	private Date endDateAndTime;
	private int participantsNumber;
	private boolean newReservationSucceded;
	private Room selectedRoom;
	private String requestorName;
	
	public static List<Reservation> resList = new ArrayList<Reservation>();

	public Reservation(Date startDateAndTime, Date endDateAndTime, int participantsNumber, Room selectedRoom,
			String requestorName) {
		super();
		this.startDateAndTime = startDateAndTime;
		this.endDateAndTime = endDateAndTime;
		this.participantsNumber = participantsNumber;
		this.selectedRoom = selectedRoom;
		this.requestorName = requestorName;
	}
	
	public Reservation(){
		
	}

	public Date getStartDateAndTime() {
		return startDateAndTime;
	}

	public void setStartDateAndTime(Date startDateAndTime) {
		this.startDateAndTime = startDateAndTime;
	}

	public Date getEndDateAndTime() {
		return endDateAndTime;
	}

	public void setEndDateAndTime(Date endDateAndTime) {
		this.endDateAndTime = endDateAndTime;
	}

	public int getParticipantsNumber() {
		return participantsNumber;
	}

	public void setParticipantsNumber(int participantsNumber) {
		this.participantsNumber = participantsNumber;
	}

	public boolean isNewReservationSucceded() {
		return newReservationSucceded;
	}
	
	public void setNewReservationSucceded(boolean newReservationSucceded) {
		this.newReservationSucceded = newReservationSucceded;
	}

	public Room getSelectedRoom(){
		return this.selectedRoom;
	}

	public void setSelectedRoom(Room selectedRoom) {
		this.selectedRoom = selectedRoom;
	}

	public String getRequestorName() {
		return requestorName;
	}

	public void setRequestorName(String requestorName) {
		this.requestorName = requestorName;
	}

	public static List<Reservation> getResList() {
		return resList;
	}

	public static void setResList(List<Reservation> resList) {
		Reservation.resList = resList;
	}

	public void setSelectedRoom(String string) {
		Room r = new Room();
		r.setNume(string);
	}
	
	@Override
	public String toString(){
		return "The reservation with made by " + this.requestorName + " is made for the room " + this.selectedRoom + 
				". It starts at " + this.startDateAndTime + " and ends at " + this.endDateAndTime + ". " + 
				this.participantsNumber + " people will attend it.";
	}
}
