package com.ibm.util;

import com.ibm.Operations;
import com.ibm.building.Structure;

public final class Constants {
	public static final String CREATE_ROOM_WITH_NAME_AND_CAPACITY = "createNewRoom";
	public static final String GET_CAPACITY_FOR_ROOM = "whatIsCapacityOfRoom";
	public static final String CREATE_NEW_PRINTER = "createNewPrinter";
	public static final String CREATE_NEW_PHONE = "createNewPhone";
	public static final String CREATE_NEW_BLACKBOARD = "createNewBlackboard";
	public static final String CREATE_RESERVATION = "createNewReservation";
	public static final String VIEW_RESERVED_ROOMS = "viewRooms";
	public static final String DELETE_RESERVATION = "deleteReservation";
	public static final String VIEW_AVAILABLE_ROOMS = "viewAvailableRooms";
	public static final Structure building = new Structure();
	public static final Operations operations = new Operations();
}
